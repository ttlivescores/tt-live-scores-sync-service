﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Reactive.Testing;
using Moq;
using RestSharp;
using TTLiveScores.SyncService.Application.Infrastructure;
using TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector;
using TTLiveScores.SyncService.Application.Infrastructure.TTLiveScoresClient;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;
using TTLiveScores.SyncService.Application.Scores;
using Xunit;

namespace TTLiveScores.SyncService.Application.Tests
{
    public class TtMatchesSyncServiceTests
    {
        [Fact]
        public void SyncShouldBeCalledEveryTwoSeconds()
        {
            var matchConnector = new Mock<IExternalTtInputConnector>();
            var ttScoresClient = new Mock<ITtScoresClient>();
            var logger = new Mock<ILogger>();

            var minuteInTicks = new TimeSpan(0, 0, 1, 0).Ticks;
            var scheduler = new TestScheduler();

            var scores = new List<Score> { new Score() { MatchId = "ABCD", HomeScore = 1, AwayScore = 3 } };

            matchConnector.Setup(c => c.GetActiveScores()).Returns(scores);

            var service = new ScoresSyncService(matchConnector.Object, ttScoresClient.Object, scheduler, logger.Object);

            service.Execute(new StartScorePolling());

            scheduler.AdvanceBy(minuteInTicks);
            ttScoresClient.Verify(s => s.SendScores(It.IsAny<List<Score>>()), Times.Exactly(30));
        }

        [Fact]
        public void SyncShouldIgnoreWhenScoresServiceIsDown()
        {
            var matchConnector = new Mock<IExternalTtInputConnector>();
            var ttScoresClient = new Mock<ITtScoresClient>();
            var logger = new Mock<ILogger>();

            var minuteInTicks = new TimeSpan(0, 0, 1, 0).Ticks;
            var scheduler = new TestScheduler();

            var scores = new List<Score> { new Score() { MatchId = "ABCD", HomeScore = 1, AwayScore = 3 } };

            matchConnector.Setup(c => c.GetActiveScores()).Returns(scores);
            ttScoresClient.Setup(c => c.SendScores(It.IsAny<IList<Score>>())).Throws(new Exception("Error in Request"));

            var service = new ScoresSyncService(matchConnector.Object, ttScoresClient.Object, scheduler, logger.Object);

            service.Execute(new StartScorePolling());
            scheduler.AdvanceBy(minuteInTicks);
        }

        [Fact]
        public void SyncStopPollingMustStopSync()
        {
            var matchConnector = new Mock<IExternalTtInputConnector>();
            var ttScoresClient = new Mock<ITtScoresClient>();
            var logger = new Mock<ILogger>();

            var minuteInTicks = new TimeSpan(0, 0, 1, 0).Ticks;
            var scheduler = new TestScheduler();

            var scores = new List<Score> { new Score() { MatchId = "ABCD", HomeScore = 1, AwayScore = 3 } };

            matchConnector.Setup(c => c.GetActiveScores()).Returns(scores);
            ttScoresClient.Setup(c => c.SendScores(It.IsAny<IList<Score>>())).Throws(new Exception("Error in Request"));

            var service = new ScoresSyncService(matchConnector.Object, ttScoresClient.Object, scheduler, logger.Object);

            service.Execute(new StartScorePolling());
            scheduler.AdvanceBy(minuteInTicks);

            ttScoresClient.Verify(s => s.SendScores(It.IsAny<List<Score>>()), Times.Exactly(30));
            ttScoresClient.ResetCalls();

            service.Execute(new StopScorePolling());
            scheduler.AdvanceBy(minuteInTicks);
            ttScoresClient.Verify(s => s.SendScores(It.IsAny<List<Score>>()), Times.Never);
        }
    }
}