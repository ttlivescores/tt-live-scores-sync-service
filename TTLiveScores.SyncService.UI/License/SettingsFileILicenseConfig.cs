﻿using System;
using TTLiveScores.SyncService.Application;
using TTLiveScores.SyncService.Application.Infrastructure;
using TTLiveScores.SyncService.Application.Infrastructure.Cqrs;
using TTLiveScores.SyncService.Application.License;
using TTLiveScores.SyncService.UI.Properties;

namespace TTLiveScores.SyncService.UI.License
{
    public class SettingsFileILicenseConfig : ILicenseConfig
    {
        private readonly IQueryMediator _queryMediator;
        private readonly ILogger _logger;

        public SettingsFileILicenseConfig(IQueryMediator queryMediator, ILogger logger)
        {
            _queryMediator = queryMediator;
            _logger = logger;
        }

        public string LicenseKey => Settings.Default.LicenseKey;

        public bool UpdateLicenseKey(string licenseKey)
        {
            if (!ValidateLicenseKey(licenseKey)) return false;

            Settings.Default.LicenseKey = licenseKey;
            Settings.Default.Save();

            return true;
        }
        
        public bool ValidateLicenseKey(string newLicenseKey)
        {
            try
            {
                var club = _queryMediator.Query<GetClubForLicense, string>(new GetClubForLicense(newLicenseKey));
                return club != null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while validating License");
                return false;
            }
        }
    }
}