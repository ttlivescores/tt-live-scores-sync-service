﻿namespace TTLiveScores.SyncService.UI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblLastSyncTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpLicense = new System.Windows.Forms.GroupBox();
            this.btnActivate = new System.Windows.Forms.LinkLabel();
            this.imgLicenseStatus = new System.Windows.Forms.PictureBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.txtLicense = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.grpLicense.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgLicenseStatus)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Location = new System.Drawing.Point(0, 524);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1101, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblLastSyncTime
            // 
            this.lblLastSyncTime.Name = "lblLastSyncTime";
            this.lblLastSyncTime.Size = new System.Drawing.Size(36, 20);
            this.lblLastSyncTime.Text = "N/A";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(27, 28);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(100, 28);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(151, 28);
            this.btnStop.Margin = new System.Windows.Forms.Padding(4);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(100, 28);
            this.btnStop.TabIndex = 2;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "TT Livescores sync service";
            this.notifyIcon1.BalloonTipTitle = "TT Livescores sync service";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "TT Scores Sync service";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grpLicense);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(812, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(289, 524);
            this.panel1.TabIndex = 3;
            // 
            // grpLicense
            // 
            this.grpLicense.Controls.Add(this.btnActivate);
            this.grpLicense.Controls.Add(this.imgLicenseStatus);
            this.grpLicense.Controls.Add(this.btnTest);
            this.grpLicense.Controls.Add(this.txtLicense);
            this.grpLicense.Location = new System.Drawing.Point(7, 12);
            this.grpLicense.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpLicense.Name = "grpLicense";
            this.grpLicense.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grpLicense.Size = new System.Drawing.Size(269, 112);
            this.grpLicense.TabIndex = 0;
            this.grpLicense.TabStop = false;
            this.grpLicense.Text = "License";
            // 
            // btnActivate
            // 
            this.btnActivate.AutoSize = true;
            this.btnActivate.Location = new System.Drawing.Point(15, 74);
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.Size = new System.Drawing.Size(219, 17);
            this.btnActivate.TabIndex = 3;
            this.btnActivate.TabStop = true;
            this.btnActivate.Text = "Klik hier om je licentie op te halen";
            this.btnActivate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnActivate_LinkClicked);
            // 
            // imgLicenseStatus
            // 
            this.imgLicenseStatus.Location = new System.Drawing.Point(140, 30);
            this.imgLicenseStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.imgLicenseStatus.Name = "imgLicenseStatus";
            this.imgLicenseStatus.Size = new System.Drawing.Size(29, 30);
            this.imgLicenseStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgLicenseStatus.TabIndex = 2;
            this.imgLicenseStatus.TabStop = false;
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(175, 30);
            this.btnTest.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(89, 30);
            this.btnTest.TabIndex = 1;
            this.btnTest.Text = "Valideer";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // txtLicense
            // 
            this.txtLicense.Location = new System.Drawing.Point(19, 34);
            this.txtLicense.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLicense.Name = "txtLicense";
            this.txtLicense.Size = new System.Drawing.Size(116, 22);
            this.txtLicense.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(0, 86);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(812, 438);
            this.textBox1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnStop);
            this.panel2.Controls.Add(this.btnStart);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(812, 86);
            this.panel2.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1101, 546);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.Text = "TT Live Scores Sync service";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.panel1.ResumeLayout(false);
            this.grpLicense.ResumeLayout(false);
            this.grpLicense.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgLicenseStatus)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblLastSyncTime;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox grpLicense;
        private System.Windows.Forms.LinkLabel btnActivate;
        private System.Windows.Forms.PictureBox imgLicenseStatus;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.TextBox txtLicense;
    }
}

