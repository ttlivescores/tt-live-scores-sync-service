﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Serilog.Events;
using TTLiveScores.SyncService.Application;
using TTLiveScores.SyncService.Application.Infrastructure;
using TTLiveScores.SyncService.Application.Infrastructure.Cqrs;
using TTLiveScores.SyncService.Application.License;
using TTLiveScores.SyncService.Application.Lineups;
using TTLiveScores.SyncService.Application.Scores;
using TTLiveScores.SyncService.UI.Properties;
using Exception = System.Exception;

namespace TTLiveScores.SyncService.UI
{
    public partial class Form1 : Form
    {
        private readonly ISubject<ILicenseEvent> _licenseEventsObservable;
        private readonly IObservable<LogEvent> _scoreSyncObservable;
        private readonly ICommandBus _commandBus;
        private readonly ILicenseConfig _licenseConfig;

        public Form1(IObservable<LogEvent> scoreSyncObservable, ICommandBus commandBus, ILicenseConfig licenseConfig)
        {
            _licenseEventsObservable = new ReplaySubject<ILicenseEvent>();
            _scoreSyncObservable = scoreSyncObservable;
            _commandBus = commandBus;
            _licenseConfig = licenseConfig;

            InitializeComponent();

            txtLicense.Text = _licenseConfig.LicenseKey;
            _licenseEventsObservable.Subscribe(HandleLicenseEvent);
            _licenseEventsObservable.OfType<LicenseInvalidEvent>()
                .Delay(TimeSpan.FromSeconds(30))
                .Subscribe(evt => ValidateLicenseKey());

            _scoreSyncObservable.Subscribe(UpdateStatus);

            _commandBus.Dispatch(new StartScorePolling());
            _commandBus.Dispatch(new StartLinupPolling());
            ValidateLicenseKey();
        }

        private bool _allowVisible;

        private void UpdateStatus(LogEvent logEvent)
        {
            var message = logEvent.RenderMessage();
            if (logEvent.Exception != null)
            {
                message += $"\n {logEvent.Exception.Message} \n {logEvent.Exception.StackTrace}";
            }
            if (!textBox1.Disposing && !textBox1.IsDisposed)
                AppendText(textBox1, message);
        }

        private void HandleLicenseEvent(ILicenseEvent licenseEvent)
        {
            switch (licenseEvent)
            {
                case LicenseValidEvent _:
                    StartSync();
                    HandleLicenseValidation(true);
                    break;
                case LicenseInvalidEvent _:
                    StopSync();
                    HandleLicenseValidation(false);
                    break;
            }//kTUHPJkpNm5AJtgNrtTbu5GV1PQ=
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            ValidateLicenseKey();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            StopSync();
        }

        private void StartSync()
        {
            _commandBus.Dispatch(new StartScorePolling());
            _commandBus.Dispatch(new StartLinupPolling());
        }

        private void StopSync()
        {
            _commandBus.Dispatch(new StopScorePolling());
            _commandBus.Dispatch(new StopLineupPolling());
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Minimize();
            }
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;

        }

        private void Minimize()
        {
            notifyIcon1.Visible = true;
            ShowInTaskbar = false;
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            _allowVisible = true;
            ShowInTaskbar = true;
            notifyIcon1.Visible = false;
            WindowState = FormWindowState.Normal;
            Show();
        }

        protected override void SetVisibleCore(bool value)
        {
            if (!_allowVisible)
            {
                value = false;
                if (!IsHandleCreated) CreateHandle();
            }
            base.SetVisibleCore(value);
        }

        // This delegate enables asynchronous calls for setting
        // the text property on a TextBox control.
        delegate void SetTextCallback(TextBox textBox, string text);

        // This method demonstrates a pattern for making thread-safe
        // calls on a Windows Forms control. 
        //
        // If the calling thread is different from the thread that
        // created the TextBox control, this method creates a
        // SetTextCallback and calls itself asynchronously using the
        // Invoke method.
        //
        // If the calling thread is the same as the thread that created
        // the TextBox control, the Text property is set directly. 

        private void AppendText(TextBox textBox, string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (textBox.InvokeRequired)
            {
                SetTextCallback d = AppendText;
                Invoke(d, textBox, text);
            }
            else
            {
                textBox.AppendText(text + Environment.NewLine);
            }
        }

        private void btnActivate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(ConfigurationManager.AppSettings["TTLiveScores:activationUrl"]);
        }
        private void btnTest_Click(object sender, EventArgs e)
        {
            ValidateLicenseKey();
        }

        private void ValidateLicenseKey()
        {
            try
            {
                var valid = _licenseConfig.ValidateLicenseKey(txtLicense.Text);

                if (valid)
                {
                    _licenseEventsObservable.OnNext(new LicenseValidEvent());
                }
                else
                {
                    _licenseEventsObservable.OnNext(new LicenseInvalidEvent());
                }
            }
            catch (Exception)
            {
                _licenseEventsObservable.OnNext(new LicenseInvalidEvent());
            }
        }

        private void HandleLicenseValidation(bool valid)
        {
            imgLicenseStatus.Image = valid ? Resources.valid : Resources.invalid;
            notifyIcon1.Icon = valid ? Resources.Live_Sync : Resources.Live_SyncError;
            if (!valid)
                StopSync();
        }

        private interface ILicenseEvent
        {
        }

        private class LicenseValidEvent : ILicenseEvent
        {

        }

        private class LicenseInvalidEvent : ILicenseEvent
        {

        }
    }
}
