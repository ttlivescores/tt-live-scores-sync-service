﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using System.Windows.Forms;
using Serilog;
using Serilog.Events;
using TTLiveScores.SyncService.Application;
using TTLiveScores.SyncService.Application.Infrastructure;
using TTLiveScores.SyncService.Application.Infrastructure.Cqrs;
using TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector;
using TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector.PieterP;
using TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector.TTMatches;
using TTLiveScores.SyncService.Application.Infrastructure.TTLiveScoresClient;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;
using TTLiveScores.SyncService.Application.License;
using TTLiveScores.SyncService.Application.Lineups;
using TTLiveScores.SyncService.Application.Scores;
using TTLiveScores.SyncService.UI.License;

namespace TTLiveScores.SyncService.UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Environment.SetEnvironmentVariable("BASEDIR", AppDomain.CurrentDomain.BaseDirectory);
            
            var logSubject = new ReplaySubject<LogEvent>();
            var seriLog = new LoggerConfiguration().ReadFrom.AppSettings()
                .WriteTo.Observers(events => events.Subscribe(logSubject)).CreateLogger();
            var logger = new SerilogLoggerAdapter(seriLog);

            logger.Debug("Starting application");

            var queryMediator = new Queries();
            var commandBus = new Commands();

            var serviceApiPath = ConfigurationManager.AppSettings["TTLiveScoresApi:baseUrl"];

            var licenseConfig = new SettingsFileILicenseConfig(queryMediator, logger);
            var connector = new LoggedExternalTtInputConnector(LoadInputConnector(logger), logger);
            var ttScoresClient = new TtScoresCachedClient(new TtLiveScoresRestServiceClient(serviceApiPath, licenseConfig, logger));
            var service = new ScoresSyncService(connector, ttScoresClient, DefaultScheduler.Instance, logger);
            var lineupsService = new LineupSyncService(connector, ttScoresClient, DefaultScheduler.Instance, logger);

            Queries.Register<GetClubForLicense, string>(x => new UserProfileHandler(ttScoresClient).Query(x));

            Commands.Register<StartScorePolling>(service.Execute);
            Commands.Register<StopScorePolling>(service.Execute);
            Commands.Register<StartLinupPolling>(lineupsService.Execute);
            Commands.Register<StopLineupPolling>(lineupsService.Execute);

            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
            System.Windows.Forms.Application.Run(new Form1(logSubject, commandBus, licenseConfig));

            logger.Debug("Stopping application");
        }

        private static IExternalTtInputConnector LoadInputConnector(SerilogLoggerAdapter logger)
        {
            switch (ConfigurationManager.AppSettings["TTInputConnector"])
            {
                case "PieterP":
                    return new PieterPTtInputConnector(ConfigurationManager.AppSettings["PieterP:installDirectory"], logger);

                case "TtMatches":
                    var dbDirectory = Path.Combine(ConfigurationManager.AppSettings["TTMatches:dbDirectory"],
                        ConfigurationManager.AppSettings["TTMatches:db"]);
                    return new TtMatchesTtInputConnector(dbDirectory);

                default:
                    return new FakeExternalTtInputConnector();
            }
        }
        public class Commands : ICommandBus
        {
            private static readonly Dictionary<Type, Action<ICommand>> Handlers =
            new Dictionary<Type, Action<ICommand>>();
            public static void Register<T>(Action<T> handler) where T : ICommand
            {
                Handlers.Add(typeof(T), x => handler((T)x));
            }

            public void Dispatch(ICommand command)
            {
                Handlers[command.GetType()](command);
            }
        }

        public class Queries : IQueryMediator
        {
            private static readonly Dictionary<Type, Func<IQuery, object>> Handlers =
            new Dictionary<Type, Func<IQuery, object>>();
            public static void Register<TQuery, TResult>(Func<TQuery, TResult> handler) where TQuery : IQuery
            {
                Handlers.Add(typeof(TQuery), x => handler((TQuery)x));
            }

            public TResult Query<TQuery, TResult>(TQuery query) where TQuery : IQuery
            {
                return (TResult)Handlers[query.GetType()](query);
            }
        }
    }
}
