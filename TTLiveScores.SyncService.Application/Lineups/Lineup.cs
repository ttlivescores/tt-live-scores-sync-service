﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace TTLiveScores.SyncService.Application.Lineups
{
    public class Lineup : IEquatable<Lineup>
    {
        public string MatchId { get; set; }

        public ICollection<LineupPlayer> LineupPlayers { get; set; }

        public bool Equals(Lineup other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(MatchId, other.MatchId) && LineupPlayers?.SequenceEqual(other.LineupPlayers) == true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Lineup) obj);
        }

        public override string ToString()
        {
            return $"{MatchId} {string.Join(",", LineupPlayers.Where(p=>p.IsHome))} {string.Join(",", LineupPlayers.Where(p => ! p.IsHome))}";
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((MatchId != null ? MatchId.GetHashCode() : 0) * 397) ^ (LineupPlayers != null ? LineupPlayers.GetHashCode() : 0);
            }
        }
    }

    public class LineupPlayer : IEquatable<LineupPlayer>
    {
        public string PlayerId { get; set; }

        public bool IsHome { get; set; }

        public int Position { get; set; }
        public bool Aanwezig { get; set; }

        public bool Equals(LineupPlayer other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(PlayerId, other.PlayerId) && IsHome == other.IsHome && Position == other.Position && Aanwezig == other.Aanwezig;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((LineupPlayer) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (PlayerId != null ? PlayerId.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ IsHome.GetHashCode();
                hashCode = (hashCode * 397) ^ Position;
                hashCode = (hashCode * 397) ^ Aanwezig.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return $"{PlayerId}";
        }
    }
}