﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using TTLiveScores.SyncService.Application.Infrastructure;
using TTLiveScores.SyncService.Application.Infrastructure.Cqrs;
using TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector;
using TTLiveScores.SyncService.Application.Infrastructure.TTLiveScoresClient;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;

namespace TTLiveScores.SyncService.Application.Lineups
{
    public class LineupSyncService : ICommandHandler<StartLinupPolling>, ICommandHandler<StopLineupPolling>
    {
        private readonly IExternalTtInputConnector _externalTtInputConnector;
        private readonly ITtScoresClient _ttScoresClient;
        private readonly IScheduler _scheduler;
        private readonly ILogger _logger;

        private bool _syncEnabled;
        private IObservable<IList<Lineup>> _lineupPollingObservable;

        public LineupSyncService(IExternalTtInputConnector externalTtInputConnector, ITtScoresClient ttScoresClient, IScheduler scheduler, ILogger logger)
        {
            _externalTtInputConnector = externalTtInputConnector;
            _ttScoresClient = ttScoresClient;
            _scheduler = scheduler;
            _logger = logger;
        }

        public void Execute(StartLinupPolling command)
        {
            if (!_syncEnabled) ToggleSync();

            _lineupPollingObservable = Observable.Interval(TimeSpan.FromSeconds(20), _scheduler)
                    .Select(_ => _externalTtInputConnector.GetActiveLinups().ToList())
                    .Timeout(TimeSpan.FromSeconds(30))
                    .Retry(5);    //Loop on errors

            _lineupPollingObservable.Subscribe(SyncLineups, HandleError);
        }

        private void SyncLineups(IList<Lineup> lineups)
        {
            if (!_syncEnabled) return;
            try
            {
               _ttScoresClient.SendLineups(lineups);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Fout tijdens SyncScores: {0}", ex.Message);
            }
        }

        private void HandleError(Exception ex)
        {
            _logger.Error(ex, "Error while fetching lineups");
            ToggleSync();
        }

        public void Execute(StopLineupPolling command)
        {
            if (_syncEnabled) ToggleSync();
        }

        private void ToggleSync()
        {
            _logger.Debug(_syncEnabled ? "Stopped lineups sync" : "Enabled lineups sync");

            _syncEnabled = !_syncEnabled;
        }
    }
}