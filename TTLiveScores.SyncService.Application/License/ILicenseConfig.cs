﻿namespace TTLiveScores.SyncService.Application.License
{
    public interface ILicenseConfig
    {
        string LicenseKey { get;}
        bool ValidateLicenseKey(string licenseKey);
        bool UpdateLicenseKey(string licenseKey);
    }
}