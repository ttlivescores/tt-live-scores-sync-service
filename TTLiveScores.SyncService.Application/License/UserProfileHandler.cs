﻿using TTLiveScores.SyncService.Application.Infrastructure.Cqrs;
using TTLiveScores.SyncService.Application.Infrastructure.TTLiveScoresClient;

namespace TTLiveScores.SyncService.Application.License
{
    public class UserProfileHandler : IQueryHandler<GetClubForLicense,string>
    {
        private readonly ITtScoresClient _ttScoresClient;

        public UserProfileHandler(ITtScoresClient ttScoresClient)
        {
            _ttScoresClient = ttScoresClient;
        }

        public string Query(GetClubForLicense query)
        {
            return _ttScoresClient.GetClubForLicense(query.License);
        }
    }
}