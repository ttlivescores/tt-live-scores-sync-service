﻿using TTLiveScores.SyncService.Application.Infrastructure.Cqrs;

namespace TTLiveScores.SyncService.Application.License
{
    public class GetClubForLicense : IQuery
    {
        public GetClubForLicense(string license)
        {
            License = license;
        }

        public string License { get;} 
    }
}