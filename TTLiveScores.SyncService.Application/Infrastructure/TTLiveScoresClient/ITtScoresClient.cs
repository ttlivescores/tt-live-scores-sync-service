﻿using System.Collections.Generic;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;
using TTLiveScores.SyncService.Application.Lineups;

namespace TTLiveScores.SyncService.Application.Infrastructure.TTLiveScoresClient
{
    public interface ITtScoresClient
    {
        void SendScores(ICollection<Score> scores);

        void SendLineups(ICollection<Lineup> lineups);
        string GetClubForLicense(string licenseKey);
    }
}