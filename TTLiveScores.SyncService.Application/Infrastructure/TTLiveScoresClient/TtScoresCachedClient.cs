﻿using System.Collections.Generic;
using System.Linq;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;
using TTLiveScores.SyncService.Application.Lineups;

namespace TTLiveScores.SyncService.Application.Infrastructure.TTLiveScoresClient
{
    public class TtScoresCachedClient : ITtScoresClient
    {
        private readonly ITtScoresClient _ttScoresClient;
        private readonly IDictionary<string, Score> sentScores = new Dictionary<string, Score>();
        private readonly IDictionary<string, Lineup> sentLineups = new Dictionary<string, Lineup>();

        public TtScoresCachedClient(ITtScoresClient ttScoresClient)
        {
            _ttScoresClient = ttScoresClient;
        }

        public void SendScores(ICollection<Score> scores)
        {
            var unCachedScores = scores.Where(s => !sentScores.ContainsKey(s.MatchId) || !s.Equals(sentScores[s.MatchId])).ToList();
            _ttScoresClient.SendScores(unCachedScores);

            foreach (var score in unCachedScores)
            {
                if (!sentScores.ContainsKey(score.MatchId))
                    sentScores.Add(score.MatchId, score);
                else
                    sentScores[score.MatchId] = score;
            }

        }

        public void SendLineups(ICollection<Lineup> lineups)
        {
            var unCachedLineups = lineups.Where(s => !sentLineups.ContainsKey(s.MatchId) || !s.Equals(sentLineups[s.MatchId])).ToList();
            _ttScoresClient.SendLineups(unCachedLineups);

            foreach (var lineup in unCachedLineups)
            {
                if (!sentLineups.ContainsKey(lineup.MatchId))
                    sentLineups.Add(lineup.MatchId, lineup);
                else
                    sentLineups[lineup.MatchId] = lineup;
            }
        }

        public string GetClubForLicense(string licenseKey)
        {
            return _ttScoresClient.GetClubForLicense(licenseKey);
        }
    }
}