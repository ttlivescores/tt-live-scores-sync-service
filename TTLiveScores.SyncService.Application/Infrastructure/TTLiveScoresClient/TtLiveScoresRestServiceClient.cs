﻿using System;
using System.Collections.Generic;
using System.Net;
using RestSharp;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;
using TTLiveScores.SyncService.Application.License;
using TTLiveScores.SyncService.Application.Lineups;

namespace TTLiveScores.SyncService.Application.Infrastructure.TTLiveScoresClient
{
    public class TtLiveScoresRestServiceClient : ITtScoresClient
    {
        private readonly ILicenseConfig _licenseConfig;
        private readonly ILogger _logger;
        private readonly RestClient _client;

        public TtLiveScoresRestServiceClient(string serviceBaseUrl, ILicenseConfig licenseConfig, ILogger logger)
        {
            _licenseConfig = licenseConfig;
            _logger = logger;
            _client = new RestClient(serviceBaseUrl);
        }

        public void SendScores(ICollection<Score> scores)
        {
            if (scores.Count == 0)
                return;

            var request = new RestRequest
            {
                Resource = "scores",
                RootElement = "scores"
            };

            request.AddJsonBody(scores);
            AddSecurityHeader(request);
            request.Method = Method.POST;

            var response = _client.Post(request);
            ThrowErrorIfInvalidResult(response, HttpStatusCode.OK, "Error when sending scores");

            _logger.Debug("Synced Scores successfully:{0}", scores);
        }

        public void SendLineups(ICollection<Lineup> lineups)
        {
            if (lineups.Count == 0)
                return;

            var request = new RestRequest
            {
                Resource = "lineups",
                RootElement = "lineups"
            };

            request.AddJsonBody(lineups);
            AddSecurityHeader(request);
            request.Method = Method.POST;

            var response = _client.Post(request);
            ThrowErrorIfInvalidResult(response, HttpStatusCode.OK, "Error when sending lineups");

            _logger.Debug("Synced Lineups successfully:{0}", lineups);
        }

        public string GetClubForLicense(string licenseKey)
        {
            var request = new RestRequest
            {
                Resource = "users/my/club/",
            };

            AddSecurityHeader(request, licenseKey);
            var response = _client.Get(request);
            ThrowErrorIfInvalidResult(response, HttpStatusCode.OK, "Error when retrieving club for license key");

            return response.Content;
        }

        private void AddSecurityHeader(IRestRequest request, string licenseKey = null)
        {
            licenseKey = licenseKey ?? _licenseConfig.LicenseKey;
            request.AddHeader("Authorization", $"SyncService {licenseKey}");
        }

        private void ThrowErrorIfInvalidResult(IRestResponse response, HttpStatusCode statusCode, string errorMessage)
        {
            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != statusCode)
            {
                throw new Exception(
                    $"{errorMessage}: status={response.StatusCode}; {response.ErrorMessage}",
                    response.ErrorException);
            }
        }
    }
}