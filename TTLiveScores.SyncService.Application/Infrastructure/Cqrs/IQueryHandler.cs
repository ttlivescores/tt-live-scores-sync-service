﻿namespace TTLiveScores.SyncService.Application.Infrastructure.Cqrs
{
    public interface IQueryHandler<TRequest, TResponse> where TRequest : IQuery
    {
        TResponse Query(TRequest query);
    }
}