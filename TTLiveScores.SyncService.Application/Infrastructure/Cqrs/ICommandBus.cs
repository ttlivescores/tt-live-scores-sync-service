﻿namespace TTLiveScores.SyncService.Application.Infrastructure.Cqrs
{
    public interface ICommandBus
    {
        void Dispatch(ICommand command);
    }
}