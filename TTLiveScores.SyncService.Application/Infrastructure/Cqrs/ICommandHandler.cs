﻿namespace TTLiveScores.SyncService.Application.Infrastructure.Cqrs
{
    public interface ICommandHandler<in TCommand> where TCommand : ICommand
    {
        void Execute(TCommand command);
    }
}