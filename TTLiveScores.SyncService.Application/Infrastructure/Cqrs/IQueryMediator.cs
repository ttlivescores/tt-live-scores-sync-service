﻿namespace TTLiveScores.SyncService.Application.Infrastructure.Cqrs
{
    public interface IQueryMediator
    {
        TResult Query<TQuery,TResult>(TQuery query) where TQuery : IQuery;
    }
}