﻿using System;
using TTLiveScores.SyncService.Application.Infrastructure.Cqrs;

namespace TTLiveScores.SyncService.Application.Infrastructure
{
    public class LoggedCommandHandler<TCommand> : ICommandHandler<TCommand> where TCommand : ICommand
    {
        private readonly ICommandHandler<TCommand> _commandHandler;
        private readonly ILogger _logger;

        public LoggedCommandHandler(ICommandHandler<TCommand> commandHandler, ILogger logger)
        {
            _commandHandler = commandHandler;
            _logger = logger;
        }
        public void Execute(TCommand command)
        {
            try
            {
                _commandHandler.Execute(command);
            }
            catch (Exception ex)
            {
                _logger.Error(ex,"Error in commandHandler");
            }
        }
    }
}