using System;
using System.Collections.Generic;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;
using TTLiveScores.SyncService.Application.Lineups;

namespace TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector
{
    public class LoggedExternalTtInputConnector : IExternalTtInputConnector
    {
        private readonly IExternalTtInputConnector _connector;
        private readonly ILogger _logger;

        public LoggedExternalTtInputConnector(IExternalTtInputConnector connector, ILogger logger)
        {
            _connector = connector;
            _logger = logger;
        }

        public IEnumerable<Score> GetActiveScores()
        {
            try
            {
                return _connector.GetActiveScores();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while fetching Active Scores");
                throw;
            }
        }

        public IEnumerable<Lineup> GetActiveLinups()
        {
            try
            {
                return _connector.GetActiveLinups();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while fetching Lineups");
                throw;
            }
        }
    }
}