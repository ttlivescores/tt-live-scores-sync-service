﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;
using TTLiveScores.SyncService.Application.Lineups;

namespace TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector.TTMatches
{
	public class TtMatchesTtInputConnector : IExternalTtInputConnector
	{
		private readonly string _connectionString;
		private const string ActiveClubsQuery = @"select c.ClubNr, c.Provincie, p.Afkorting, c.ClubNaam
from (clubs c
inner join opties o on c.ClubNr = o.Item  )
inner join Provincies p on c.Provincie = p.Nr
where o.soort = 'ClubNr'";

		private const string ActiveScoresQuery = @"select m.WEDSTRNR
		,m.FORFAITTH,m.FORFAITUIT
		,m.W01,m.W02,m.W03,m.W04,m.W05,m.W06,m.W07,m.W08,m.W09,m.W10,m.W11,m.W12,m.W13,m.W14,m.W15,m.W16
from matchen m
where (m.LKTHUIS = @LKTHUIS and m.PROVTHUIS = @PROVTHUIS) 
and DATUM = @DATUM";

        private const string ActiveLineupsQuery = @"select m.WEDSTRNR
		,m.THUISSP1, m.AFWTH1,m.THUISSP2, m.AFWTH2,m.THUISSP3, m.AFWTH3,m.THUISSP4, m.AFWTH4
		,m.UITSP1, m.AFWUIT1,m.UITSP2, m.AFWUIT2,m.UITSP3, m.AFWUIT3,m.UITSP4, m.AFWUIT4
		
from matchen m
where (m.LKTHUIS = @LKTHUIS and m.PROVTHUIS = @PROVTHUIS) 
and DATUM = @DATUM";


		private ClubInfo _activeClub;

		public TtMatchesTtInputConnector(string dbPath)
		{
			_connectionString = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={dbPath}";
		}

		public ClubInfo GetActiveClub()
		{
			if (_activeClub != null)
				return _activeClub;

			var command = new OleDbCommand(ActiveClubsQuery);

			using (OleDbConnection myConnection = new OleDbConnection())
			{
				myConnection.ConnectionString = _connectionString;
				myConnection.Open();

				command.Connection = myConnection;
				using (var reader = command.ExecuteReader())
				{
					if (!reader.HasRows)
						return null;
					reader.Read();

					_activeClub = new ClubInfo
					{
						ClubNr = int.Parse(reader.GetString(0)),
						ProvincieNr = reader.GetInt32(1),
						ProvincieAfkorting = reader.GetString(2),
						ClubNaam = reader.GetString(3)
					};
					return _activeClub;
				}
			}
		}

		public IEnumerable<Score> GetActiveScores()
		{
			var clubInfo = GetActiveClub();

			var command = new OleDbCommand(ActiveScoresQuery);
			command.Parameters.AddWithValue("@LKTHUIS", clubInfo.ClubNr.ToString());
			command.Parameters.AddWithValue("@PROVTHUIS", clubInfo.ProvincieNr.ToString());
			command.Parameters.AddWithValue("@DATUM", DateTime.Now.ToString("dd-MM-yyyy"));

			using (OleDbConnection myConnection = new OleDbConnection())
			{
				myConnection.ConnectionString = _connectionString;
				myConnection.Open();

				//execute queries, etc
				command.Connection = myConnection;
				using (var trans = myConnection.BeginTransaction(IsolationLevel.ReadUncommitted))
				{
					command.Transaction = trans;

					using (var reader = command.ExecuteReader())
					{
						if (!reader.HasRows)
							yield break;
						while (reader.Read())
							yield return new TtMatchesScoreParser().ParseScores(reader);
					}
				}
			}
		}

	    public IEnumerable<Lineup> GetActiveLinups()
	    {
            var clubInfo = GetActiveClub();

            var command = new OleDbCommand(ActiveLineupsQuery);
            command.Parameters.AddWithValue("@LKTHUIS", clubInfo.ClubNr.ToString());
            command.Parameters.AddWithValue("@PROVTHUIS", clubInfo.ProvincieNr.ToString());
            command.Parameters.AddWithValue("@DATUM", DateTime.Now.ToString("dd-MM-yyyy"));

            using (OleDbConnection myConnection = new OleDbConnection())
            {
                myConnection.ConnectionString = _connectionString;
                myConnection.Open();

                //execute queries, etc
                command.Connection = myConnection;
                using (var trans = myConnection.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    command.Transaction = trans;

                    using (var reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows)
                            yield break;
                        while (reader.Read())
                            yield return new TtMatchesLinupParser().ParseLineups(reader);
                    }
                }
            }
        } 


	}
}