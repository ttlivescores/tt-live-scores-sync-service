﻿using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using TTLiveScores.SyncService.Application.Lineups;

namespace TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector.TTMatches
{
    public class TtMatchesLinupParser
    {
        public Lineup ParseLineups(OleDbDataReader reader)
        {
            var lineUp = new Lineup { MatchId = reader.GetString(0) };

            lineUp.LineupPlayers = ParseLineupPlayers(reader).ToList();

            return lineUp;
        }

        private IEnumerable<LineupPlayer> ParseLineupPlayers(OleDbDataReader reader)
        {
            if (!reader.HasRows)
            {
                yield break;
            }

            for (var i = 1; i < reader.FieldCount; i = i + 2)
            {
                var lineupPlayer = new LineupPlayer();
                if (reader.IsDBNull(i) || reader.IsDBNull(i + 1))
                    continue;

                lineupPlayer.PlayerId = reader.GetString(i);
                
                lineupPlayer.Aanwezig = reader.GetString(i + 1).Equals("N");
                lineupPlayer.Position = i / 2;
                lineupPlayer.IsHome = reader.GetName(i).Contains("THUIS");

                yield return lineupPlayer;
            }
        }
    }
}