﻿using System.Data.OleDb;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;

namespace TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector.TTMatches
{
    public class TtMatchesScoreParser
    {
        private const int MaxMatchCount = 16;
        private const int HomeForfaitColumn = 1;
        private const int AwayForfaitColumn = 2;
        private const int MatchStartColumn = 3;

        public Score ParseScores(OleDbDataReader reader)
        {
            var score = new Score { MatchId = reader.GetString(0) };
            
            if (reader.GetString(HomeForfaitColumn) == "J")
            {
                score.HomeScore = 0;
                score.AwayScore = 16;
                return score;
            }
            if (reader.GetString(AwayForfaitColumn) == "J")
            {
                score.HomeScore = 16;
                score.AwayScore = 0;
                return score;
            }

            
            for (var i = 0; i < MaxMatchCount; i++)
            {
                int currentColumn = MatchStartColumn + i;
                if (reader.IsDBNull(currentColumn)) continue;

                score.HomeScore += reader.GetString(currentColumn) == "+" ? 1 : 0;
                score.AwayScore += reader.GetString(currentColumn) == "-" ? 1 : 0;
            }

            return score;
        }
        
    }
}