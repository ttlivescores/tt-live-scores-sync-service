﻿using System.Collections.Generic;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;
using TTLiveScores.SyncService.Application.Lineups;

namespace TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector
{
    public interface IExternalTtInputConnector
    {
        IEnumerable<Score> GetActiveScores();

        IEnumerable<Lineup> GetActiveLinups();
    }
}