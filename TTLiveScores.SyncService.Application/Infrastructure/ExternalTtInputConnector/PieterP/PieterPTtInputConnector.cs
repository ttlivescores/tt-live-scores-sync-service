﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;
using TTLiveScores.SyncService.Application.Lineups;

namespace TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector.PieterP
{
    public class PieterPTtInputConnector : IExternalTtInputConnector
    {
        private readonly ILogger _logger;
        private readonly string _matchFileLocations;

        public PieterPTtInputConnector(string pieterPInstallationLocation, ILogger logger)
        {
            _logger = logger;
            _matchFileLocations = Path.Combine(pieterPInstallationLocation, "data", "official");
        }
        public IEnumerable<Score> GetActiveScores()
        {
            var activeMatches = GetActiveMatches();
            var scoreparser = new PieterPScoreParser();

            var result = new List<Score>();
            foreach (var activeMatch in activeMatches)
            {
                try
                {
                    var matchDocument = LoadMatchDocument(activeMatch);
                    if (matchDocument == null)
                        continue;
                    result.Add(scoreparser.ParseMatchScore(matchDocument));
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "matchParsing Failed: {0}", activeMatch);
                }
            }

            return result;
        }

        public IEnumerable<Lineup> GetActiveLinups()
        {
            var activeMatches = GetActiveMatches();
            var lineupParser = new PieterPLineupParser();

            var result = new List<Lineup>();
            foreach (var activeMatch in activeMatches)
            {
                try
                {
                    var matchDocument = LoadMatchDocument(activeMatch);
                    if (matchDocument == null)
                        continue;
                    result.Add(lineupParser.ParseLineups(matchDocument));
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "lineupParsing Failed: {0}", activeMatch);
                }
            }

            return result;
        }

        private IEnumerable<string> GetActiveMatches()
        {
            var filesInDirectories = Directory.EnumerateFiles(_matchFileLocations, "*.wbml", SearchOption.TopDirectoryOnly);
            return filesInDirectories.Where(f => File.GetLastWriteTime(f) > DateTime.Today.AddDays(-1)).Select(Path.GetFileNameWithoutExtension);
        }

        private XDocument LoadMatchDocument(string match)
        {
            var matchFilepath = Path.Combine(_matchFileLocations, $"{match}.wbml");
            if (!File.Exists(matchFilepath))
            {
                return null;
            }

            return XDocument.Load(matchFilepath);
        }
    }
}