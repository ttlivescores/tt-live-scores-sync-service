﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;
using TTLiveScores.SyncService.Application.Lineups;

namespace TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector.PieterP
{
    public class PieterPLineupParser
    {
        public Lineup ParseLineups(XDocument matchDocument)
        {
            var matchId = matchDocument.Root.Elements("Content").FirstOrDefault(e => "OfficieelMatchId".Equals(e.Attribute("name")?.Value))?.Attribute("value")?.Value;

            var lineup = new Lineup { MatchId = matchId };

            var thuisPlayers = GetLineuPlayers(matchDocument, true);
            var uitPlayers = GetLineuPlayers(matchDocument, false);

            lineup.LineupPlayers = thuisPlayers.Union(uitPlayers).ToList();

            return lineup;
        }

        private IEnumerable<LineupPlayer> GetLineuPlayers(XDocument matchDocument, bool thuis)
        {
            var expectedName = thuis ? "ThuisComputerNummer" : "UitComputerNummer";
            return matchDocument.Root?.Elements("Content")
                .Where(x => x.Attribute("name")?.Value.StartsWith(expectedName) == true)
                .Select(x => ToLineupPlayer(x, thuis)) ?? new List<LineupPlayer>();
        }

        private LineupPlayer ToLineupPlayer(XElement xElement, bool thuis)
        {
            var computerNummerName = xElement.Attribute("name")?.Value ?? "";
            int.TryParse(computerNummerName.Substring(computerNummerName.Length - 1), out int position);
            return new LineupPlayer
            {
                Aanwezig = true,
                IsHome = thuis,
                Position = position,
                PlayerId = xElement.Attribute("value")?.Value
            };
        }
    }
}