﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;

namespace TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector.PieterP
{
    public class PieterPScoreParser
    {
        public Score ParseMatchScore(XDocument matchDocument)
        {
            var matchId = matchDocument.Root.Elements("Content").FirstOrDefault(e => "OfficieelMatchId".Equals(e.Attribute("name")?.Value))?.Attribute("value")?.Value;

            if(IsHomeForfait(matchDocument))
                return new Score(matchId, 0,16);
            if (IsUitForfait(matchDocument))
                return new Score(matchId, 16, 0);

            var matches = matchDocument.Root.Elements("Score").GroupBy(e => e.Attribute("row")?.Value);

            int homeScore = 0;
            int awayScore = 0;

            foreach (var matchElement in matches)
            {
                var matchScore = GetMatchScore(matchElement.ToList());

                if (IsHomeWin(matchScore))
                    homeScore++;
                if (IsAwayWin(matchScore))
                    awayScore++;
            }

            return new Score(matchId, homeScore, awayScore);
        }

        private bool IsHomeForfait(XDocument matchDocument)
        {
            return IsForfait(matchDocument, "ThuisFF");
        }

        private bool IsUitForfait(XDocument matchDocument)
        {
            return IsForfait(matchDocument, "UitFF");
        }

        private bool IsForfait(XDocument matchDocument, string forfaitType)
        {
            return matchDocument.Root.Elements("Content").Any(e =>
                forfaitType.Equals(e.Attribute("name")?.Value) && "True".Equals(e.Attribute("value")?.Value));
        }

        private (int homeScore, int awayScore) GetMatchScore(List<XElement> matchGames)
        {
            var homeWo = IsHomeWo(matchGames);
            var awayWo = IsAwayWo(matchGames);
            if (homeWo && awayWo)
                return (0, 0);
            if (homeWo)
                return (0, 3);
            if (awayWo)
                return (3, 0);

            var games = matchGames.GroupBy(s => s.Attribute("col")?.Value);
            int homeScore = 0;
            int awayScore = 0;
            foreach (var game in games)
            {
                var gameScore = GetGameScore(game.ToList());
                if (gameScore.homeScore == null || gameScore.awayScore == null)
                    continue;

                if (gameScore.homeScore.Value >= Math.Max(gameScore.awayScore.Value, 11))
                {
                    homeScore++;
                    continue;
                }

                if (gameScore.awayScore.Value >= Math.Max(gameScore.homeScore.Value, 11))
                {
                    awayScore++;
                    continue;
                }
            }

            return (homeScore, awayScore);
        }

        private (int? homeScore, int? awayScore) GetGameScore(IList<XElement> game)
        {
            var homeGameEntry = ParseGameEntry(game, "Home");
            var awayGameEntry = ParseGameEntry(game, "Away");

            return (homeGameEntry, awayGameEntry);
        }

        private int? ParseGameEntry(IEnumerable<XElement> game, string type)
        {
            var entry = game.FirstOrDefault(g => type.Equals(g.Attribute("type")?.Value))?.Attribute("value");

            if (entry == null) return null;

            if (int.TryParse(entry.Value, out int entryValue))
                return entryValue;

            return null;
        }


        private bool IsHomeWo(List<XElement> matchGames)
        {
            return IsWo(matchGames, "Home");
        }

        private bool IsAwayWo(List<XElement> matchGames)
        {
            return IsWo(matchGames, "Away");
        }

        private bool IsWo(List<XElement> matchGames, string type)
        {
            return matchGames.Any(g => type.Equals(g.Attribute("type")?.Value) && "WO".Equals(g.Attribute("value")?.Value));
        }

        private bool IsHomeWin((int homeScore, int awayScore) gameScore)
        {
            return gameScore.homeScore == 3;
        }

        private bool IsAwayWin((int homeScore, int awayScore) gameScore)
        {
            return gameScore.awayScore == 3;
        }
    }
}