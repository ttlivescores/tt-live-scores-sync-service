﻿using System.Collections.Generic;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;
using TTLiveScores.SyncService.Application.Lineups;

namespace TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector
{
    public class FakeExternalTtInputConnector : IExternalTtInputConnector
    {
        public ClubInfo GetActiveClub()
        {
            return new ClubInfo { ClubNaam = "St-Pauwels", ClubNr = 125, ProvincieAfkorting = "OVL", ProvincieNr = 7 };
        }

        public IEnumerable<Score> GetActiveScores()
        {
            yield return new Score("L-12-245-1", 0, 2);
            yield return new Score("P-12-245-1", 0, 2);
            yield return new Score("P1-12-245-1", 0, 2);
            yield return new Score("P2-12-245-1", 0, 2);
        }

        public IEnumerable<Lineup> GetActiveLinups()
        {
            yield return new Lineup
            {
                MatchId = "0001",
                LineupPlayers = new LineupPlayer[]
                {
                    new LineupPlayer {PlayerId = "0001"}, 
                }
            };
        }
    }
}