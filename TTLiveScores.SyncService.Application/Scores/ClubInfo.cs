﻿namespace TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector
{
    public class ClubInfo
    {
        public int ClubNr { get; set; }

        public int ProvincieNr { get; set; }

        public string ProvincieAfkorting { get; set; }

        public string ClubNaam { get; set; } 
    }
}