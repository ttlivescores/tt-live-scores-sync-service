﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using TTLiveScores.SyncService.Application.Infrastructure;
using TTLiveScores.SyncService.Application.Infrastructure.Cqrs;
using TTLiveScores.SyncService.Application.Infrastructure.ExternalTtInputConnector;
using TTLiveScores.SyncService.Application.Infrastructure.TTLiveScoresClient;
using TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector;

namespace TTLiveScores.SyncService.Application.Scores
{
    public class ScoresSyncService : ICommandHandler<StartScorePolling>,
        ICommandHandler<StopScorePolling>
    {
        private readonly IExternalTtInputConnector _externalTtInputConnector;
        private readonly ITtScoresClient _ttScoresClient;
        private readonly IScheduler _scheduler;
        private readonly ILogger _logger;

        private bool _syncEnabled;
        private IObservable<IList<Score>> _scorePollingObservable;

        public ScoresSyncService(IExternalTtInputConnector externalTtInputConnector, ITtScoresClient ttScoresClient, IScheduler scheduler, ILogger logger)
        {
            _externalTtInputConnector = externalTtInputConnector;
            _ttScoresClient = ttScoresClient;
            _scheduler = scheduler;
            _logger = logger;
        }

        public void Execute(StartScorePolling command)
        {
            if (!_syncEnabled) ToggleSync();

            _scorePollingObservable = Observable.Interval(TimeSpan.FromSeconds(2), _scheduler)
                    .Select(_ => _externalTtInputConnector.GetActiveScores().ToList())
                    .Timeout(TimeSpan.FromSeconds(10))
                    .Retry(5);    //Loop on errors

            _scorePollingObservable.Subscribe(SyncScores, HandleError);
        }
        private void SyncScores(IList<Score> scores)
        {
            if (!_syncEnabled) return;
            try
            {
                _ttScoresClient.SendScores(scores);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Fout tijdens SyncScores: {0}", ex.Message);
            }
        }

        private void HandleError(Exception ex)
        {
            _logger.Error(ex, "Error while fetching scores");
            ToggleSync();
        }

        public void Execute(StopScorePolling command)
        {
            if (_syncEnabled) ToggleSync();
        }

        private void ToggleSync()
        {
            _logger.Debug(_syncEnabled ? "Stopped scores sync" : "Enabled scores sync");

            _syncEnabled = !_syncEnabled;
        }
        
    }
}