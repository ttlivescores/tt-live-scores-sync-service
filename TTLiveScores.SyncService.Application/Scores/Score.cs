﻿using System;

namespace TTLiveScores.SyncService.Application.Infrastructure.TTMatchesConnector
{
    public class Score : IEquatable<Score>
    {
        public Score()
        {

        }
        public Score(string matchId, int homeScore, int awayScore)
        {
            MatchId = matchId;
            HomeScore = homeScore;
            AwayScore = awayScore;
        }

        public string MatchId { get; set; }
        public int HomeScore { get; set; }

        public int AwayScore { get; set; }

        public override string ToString()
        {
            return $"{MatchId}: {HomeScore}-{AwayScore}";
        }

        public bool Equals(Score other)
        {
            return other.MatchId == MatchId && HomeScore == other.HomeScore && AwayScore == other.AwayScore;
        }
    }
}